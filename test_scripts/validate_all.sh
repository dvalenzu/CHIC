#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

./validate_fmi.sh; 
./validate_bio_normal.sh;
./validate_bio_detail.sh;
./validate_bio_detail_paired.sh;
