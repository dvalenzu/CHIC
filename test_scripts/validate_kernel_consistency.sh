#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

source "./utils.sh"
source "./bio_utils.sh"
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

./clean.sh

for FOLDER in ${DIR}/bio_data/data_*/
#for FOLDER in "${DIR}/bio_data/data_2/" "${DIR}/bio_data/data_3/" "${DIR}/bio_data/data_4/"
#for FOLDER in "${DIR}/bio_data/data_4/"
do
  for MAX_D in "5" "10" "20"
  do
    REFERENCE=${FOLDER}/genome.fa
    READS=${FOLDER}/reads.fq
    rm -f ${REFERENCE}.*  
    echo "**************************"
    echo "Testing on ${REFERENCE}"
    echo "**************************"

    PLAIN=${REFERENCE}.plain
    ## TODO: make sure we are in debug mode.
    bio_fasta_to_plain ${REFERENCE} ${PLAIN}
    ../src/chic_index ${REFERENCE} ${MAX_D} -o chic.${MAX_D}
    ../src/build_index ${PLAIN} ${MAX_D} -o plain.${MAX_D}
    grep -v ">" chic.${MAX_D}.kernel_text | tr -d '\n'  > chic.kernel_final
    cat plain.${MAX_D}.kernel_text | tr '$' 'N' > plain.kernel_final

    utils_assert_equal_files  chic.kernel_final plain.kernel_final

  done
  ./tools/validate_sub_kernels.py plain.5.kernel_text plain.10.kernel_text
  ./tools/validate_sub_kernels.py plain.10.kernel_text plain.20.kernel_text
  rm -f ${REFERENCE}.*  chic.* plain.*
done

utils_success_exit
