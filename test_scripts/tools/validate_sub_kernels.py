#!/usr/bin/env python

import os
import sys
from subprocess import call

def main(argv):
    n_args = len(sys.argv);
    if(n_args != 3):
        print 'Got ',n_args-1,' arguments, this is incorrect.'
        print 'Usage:'
        print sys.argv[0] , 'seq_1 seq_2'
        sys.exit();
    input_1=sys.argv[1];
    input_2=sys.argv[2];
    
    
    with open (input_1, "r") as myfile:
        A = myfile.read().replace('\n', '')
    with open (input_2, "r") as myfile:
        B = myfile.read().replace('\n', '')
    
    j = 0;
    for i in range(len(A)):
        if A[i] == '$':
            continue
        #print "i: " + str(i)
        #print "Looking for" + A[i]
        while (j < len(B) and B[j] != A[i]):
            j = j + 1
        if (j == len(B)):
            print "Second file exhausted before first, so A is not a subsequence of B"
            exit(33)
    
    exit(0)
    #print A
    #print B
if __name__ == "__main__":
    main(sys.argv)
